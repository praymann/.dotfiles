# BASH aliases

# Handy aliases
alias egrep=egrep --color=auto
alias fgrep=fgrep --color=auto
alias grep=grep --color=auto
alias ..='cd ..'
alias ...='cd ../..'
alias ls="ls -G"
alias ll="ls -la"
alias lt="ls -lart"

# Fancy, fancy
alias ducks='du -cksh * | sort -rn|head -11' # Lists folders and files sizes in the current folder
alias upmebaby='sudo apt-get update && sudo apt-get dist-upgrade'
