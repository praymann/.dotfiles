#!/bin/bash
############################
# install.sh
# This script creates symlinks from the home directory to any desired dotfiles in ~/.dotfiles
############################

########## Variables

dir=~/.dotfiles                    # dotfiles directory
olddir=~/.dotfiles~orig             # old dotfiles backup directory

# Deprecated. For loop now looks for a file extension via '.'
#files="bashrc vimrc bash_logout gitconfig bash_profile bash_aliases"    # list of files/folders to symlink in homedir

##########

# create dotfiles_old in homedir
if [ ! -d $olddir ]; then
    echo -e "\t$olddir is not present, creating"
    mkdir -p $olddir
fi

# change to the dotfiles directory
echo -e "\tChanging to the $dir directory"
cd $dir

# move any existing dotfiles in homedir to dotfiles_old directory, then create symlinks 
echo -e "\tMoving any existing dotfiles from ~ to $olddir"
for file in `ls $dir | grep -vF .`; do
    if [ -a ~/.$file ]; then
        if [ -h ~/.$file ]; then
            echo -e "\t\t$file is already symlinked, removing symlink"
            rm ~/.$file
        else
            echo -e "\t\tMoving old $file"
            mv ~/.$file $olddir
        fi
    fi
    echo -e "\tCreating symlink to $file in home directory."
    ln -s $dir/$file ~/.$file
done
